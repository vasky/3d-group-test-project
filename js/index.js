import './../style.css'
import './../sass/main.sass'

require("./modal.js")
require("./mobileMenu.js")
require("./slider.js")

const btnShowMoreYellowL = document.querySelector(".icon__show-more")
const hideOn768YellowL = document.querySelector(".yellow-line").querySelectorAll(".hide768")
const mobileInput = document.querySelector(".find__mobile-icon")
const mobileInputClose = mobileInput.querySelector(".close-icon")
const searchIconInputClose = mobileInput.querySelector(".searchIcon")
const logo = document.querySelector(".logo")
const exitIcon = document.querySelector(".btn__enter")
const btnCart = document.querySelector(".btn__cart")
const inputSearch = document.querySelector(".input__find-product")

btnShowMoreYellowL.addEventListener("click", () => {
    hideOn768YellowL.forEach( el => {
        el.classList.toggle("hide768")
    })
})
mobileInputClose.addEventListener("click", () => {
    mobileInput.classList.toggle("active")
    closeMobileInputSearch()
    toggleIconsInputSearch()
})
searchIconInputClose.addEventListener("click", () => {
    mobileInput.classList.toggle("active")
    toggleIconsInputSearch()
    showMobileInputSearch()
})

function toggleIconsInputSearch() {
    searchIconInputClose.classList.toggle('hide')
    mobileInputClose.classList.toggle('hide')
}
function showMobileInputSearch() {
    logo.classList.add("hide")
    exitIcon.classList.add("hide")
    btnCart.classList.add("hide")
    inputSearch.style.cssText += 'display: flex !important'
    // inputSearch.style.display = "flex"
    // inputSearch.style.display = "flex"
    inputSearch.style.right = 'auto'
    inputSearch.style.left = '40px'
    inputSearch.style.width = '80%'
    console.log(inputSearch.style.display)
}
function closeMobileInputSearch() {
    logo.classList.remove("hide")
    exitIcon.classList.remove("hide")
    btnCart.classList.remove("hide")
    inputSearch.style.display = "none "
}

//resume hidden input
const hiddenInput = document.querySelector(".choose-resume")
const btnChooseResume = document.querySelector(".send-resume__btn")

btnChooseResume.addEventListener("click", () => {
    hiddenInput.click()
})