const sliders = document.querySelectorAll(".slide")
const btnPref = document.querySelector(".btn__btn-pref")
const btnNext = document.querySelector(".btn__btn-next")
const classActiveSlide = "activeSlide"
// const amountPhotoInSlider = window.innerWidth >= 769 ? document.querySelectorAll(".activeSlide").length : 1
let amountPhotoInSlider = ""
if( window.innerWidth >= 769 ){
    amountPhotoInSlider = document.querySelectorAll(".activeSlide").length
}else{
    amountPhotoInSlider = 1
    for(let i=0; i< sliders.length; i++){
        if( i !== 0 )
            sliders[i].classList.remove(classActiveSlide)
    }
}
// const amountPhotoInSlider = document.querySelectorAll(".activeSlide").length
let currentSlideNumber = amountPhotoInSlider
console.log(window.innerWidth)
console.log(currentSlideNumber)
let directionSlider = "right"
btnPref.dataset.disabled = "true"
btnPref.classList.add("disabledButton")


btnPref.addEventListener("click", () => {
    // if(!btnPref.dataset.disabled ){
    //     currentSlideNumber -= 1
    //     if(sliders[currentSlideNumber]){
    //         sliders[currentSlideNumber].classList.add(classActiveSlide)
    //         sliders[currentSlideNumber - amountPhotoInSlider].classList.remove(classActiveSlide)
    //     }else{
    //         btnNext.dataset.disabled = "true"
    //         btnNext.classList.add("disabledButton")
    //     }
    // }

    if(btnPref.dataset.disabled !== 'true'){
        btnNext.dataset.disabled = "false"
        btnNext.classList.remove("disabledButton")
        if( directionSlider === 'left' ) {
            currentSlideNumber -= 1
            sliders[ currentSlideNumber -1].classList.add( classActiveSlide )
            sliders[currentSlideNumber + amountPhotoInSlider -1].classList.remove( classActiveSlide )

        }else{
            sliders[currentSlideNumber -1].classList.remove( classActiveSlide )
            currentSlideNumber -= amountPhotoInSlider
            sliders[ currentSlideNumber -1].classList.add( classActiveSlide )
        }
        directionSlider = "left"
        if(!sliders[currentSlideNumber -2]) {
            btnPref.dataset.disabled = "true"
            btnPref.classList.add("disabledButton")
        }
    }
})

btnNext.addEventListener("click", () => {
    // if(!btnNext.dataset.disabled ){
    //     currentSlideNumber += 1
    //     if(sliders[currentSlideNumber]){
    //         sliders[currentSlideNumber].classList.add(classActiveSlide)
    //         sliders[currentSlideNumber - amountPhotoInSlider].classList.remove(classActiveSlide)
    //     }else{
    //         btnNext.dataset.disabled = "true"
    //         btnNext.classList.add("disabledButton")
    //     }
    // }
    if(btnNext.dataset.disabled !== 'true'){
        btnPref.dataset.disabled = "false"
        btnPref.classList.remove("disabledButton")
        if( directionSlider === 'right' ) {
            currentSlideNumber += 1
            sliders[ currentSlideNumber -1].classList.add( classActiveSlide )
            sliders[currentSlideNumber - amountPhotoInSlider -1 ].classList.remove( classActiveSlide )

        }else{
            sliders[currentSlideNumber -1].classList.remove( classActiveSlide )
            currentSlideNumber += amountPhotoInSlider
            sliders[ currentSlideNumber -1].classList.add( classActiveSlide )
        }
        directionSlider = "right"
        if(!sliders[currentSlideNumber]) {
            btnNext.dataset.disabled = "true"
            btnNext.classList.add("disabledButton")
        }
    }

})