import State from './state.js'

const state = new State()
const listCities = state.cities

const modalBtnCites = document.querySelector(".btn__choose-city")
const modalWindow = document.querySelector("#modalWindow")
const inputModal = document.querySelector(".type__cities")
const modalCloseIco = modalWindow ? modalWindow.querySelector(".modal__close") : false
const currentListCities = document.querySelector(".current-cities__list")
let cityWasFound = false

modalBtnCites.addEventListener("click", () => {
    openModal()
})
modalCloseIco.addEventListener("click", () => {
    closeModal()
})
inputModal.addEventListener("change", (e) => {
    let filter = e.target.value.toUpperCase()
    cityWasFound = false
    if( filter !== '' && !cityWasFound){
        console.clear()
        currentListCities.innerHTML = ``
        listCities.forEach( city => {
            // console.log(city)
            if( city.toUpperCase().indexOf(filter) > - 1){
                // currentListCities.innerHTML +=  `<div class="current-city">${city}</div>`
                const cityBlock = makeCurrentCity(city)
                currentListCities.append(cityBlock)
                cityBlock.addEventListener("click", ()=> {
                    e.target.value = cityBlock.innerText
                    cityWasFound = true
                    currentListCities.innerHTML = ``
                })
            }
        })
    }else{
        currentListCities.innerHTML = ``
    }
})



function openModal(){
    modalWindow.classList.remove("hide")
}
function closeModal(){
    modalWindow.classList.add("hide")
}
function makeCurrentCity(city){
    const cityBlock = document.createElement("div")
    cityBlock.classList.add("current-city")
    cityBlock.textContent = city
    return cityBlock
}