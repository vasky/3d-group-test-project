const mobileMenu = document.querySelector("#mobile-menu")
const closeMenuMobileIcon = mobileMenu.querySelector(".close-menu__icon")
const mobMenu__li = document.querySelectorAll(".mobile-menu__item")
const mobMenu__title = document.querySelectorAll(".mobile-menu__item")
const btnOpenMobileMenu = document.querySelector(".open-mobile-menu")

mobMenu__li.forEach((li) => {
    const titleLi = li.querySelector(".title");
    if(li.childNodes.length > 1){
        titleLi.addEventListener( "click", () => {
            const categoryOrders = li.querySelector(".category__orders")
            categoryOrders.classList.toggle("hide")
        })
    }
})

closeMenuMobileIcon.addEventListener("click", () => {
    mobileMenu.style.display = "none"
})
btnOpenMobileMenu.addEventListener("click", () => {
    mobileMenu.style.display = "block"
})